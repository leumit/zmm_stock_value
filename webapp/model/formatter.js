sap.ui.define([

], function() {
	"use strict";

	return {
		addComma: function(value) {
			if (value) {
				value = value.toString();
				return value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			return value;
		}

	};
});