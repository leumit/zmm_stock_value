/* global cOwner : true*/
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/ui/model/Filter"
], function(JSONModel, Device,Filter) {
	"use strict";
 
	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
			createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				Filters:{}
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		loadTableData: function(){
			debugger;
			var aFilters = [new Filter("Plant", sap.ui.model.FilterOperator.EQ, '1000')];
			
			// const aFilters = [
			// 	new Filter("IvGroupByVendor", "EQ", true),
			// 	new Filter("Vendor", "EQ", sVendor),
			// 	new Filter("IvVaccineType", "EQ", sType)

			// ];
			// var sPath = cOwner.getModel("ODATA").createKey("/ZMM_STOCK_VALUE_list");
			return new Promise(function(resolve, reject) {
				cOwner.getModel("ODATA").read("/zmm_cds_stock_value", {
					filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		}

	};
});