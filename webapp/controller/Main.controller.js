/* global cOwner : true*/
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ZMM_STOCK_VALUE/model/models",
	"sap/ui/export/Spreadsheet",
	"sap/ui/model/Filter",
	'sap/ui/table/TablePersoController',
	"sap/m/MessageBox",
	"ZMM_STOCK_VALUE/model/formatter"
], function(Controller, models, Spreadsheet ,Filter , TablePersoController,MessageBox,formatter ) {
	"use strict";

	return Controller.extend("ZMM_STOCK_VALUE.controller.Main", {
		formatter:formatter,
		onInit: function() {
			cOwner._MainController = this; 
			cOwner.getModel("JSON").setProperty("/AppType",sType);
			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			cOwner._MainController.GetData();
			
		},
		onAfterRendering: function () {
			debugger;
			try{
			this.oPersonalizationService = sap.ushell.Container.getService("Personalization");

			//const sItem = sap.ushell.services.AppConfiguration.getCurrentApplication().applicationDependencies.name;
			var oPersId = {
				container: "StockValuePersonalisation", //any
				item: 'StockValue' //any- I have used the table name 
			};
			var oScope = {
				keyCategory: this.oPersonalizationService.constants.keyCategory.FIXED_KEY,
				writeFrequency: this.oPersonalizationService.constants.writeFrequency.LOW,
				clientStorageAllowed: true
			};

			// Get a Personalizer
			var oPersonalizer = this.oPersonalizationService.getPersonalizer(oPersId, oScope, cOwner);

			this._oTPC = new TablePersoController({
				table: this.getView().byId("dataTable"),
				componentName: "StockValue",
				persoService: oPersonalizer
			});
		}catch(e){}
		},
		GetData: function() {
			debugger;
			
			models.loadTableData().then(function(data) {
				debugger;
				let aStorageLocation = [];
				var CurrentStockCurValSum = 0;
				var StockInTransitCurValSum = 0;
				for (var i = 0; i < data.results.length; i++) {
					// aStorageLocation.push({"StorageLocation" :data.results[i].StorageLocation});
					// aErnam.push({"Ernam" :data.results[i].ernam});
					CurrentStockCurValSum += parseInt(data.results[i].CurrentStockCurVal);
					StockInTransitCurValSum += parseInt(data.results[i].StockInTransitCurVal);
					data.results[i].CurrentStockCurValInt = parseInt(data.results[i].CurrentStockCurVal);
					data.results[i].MaterialInt = parseInt(data.results[i].Material);	
					data.results[i].MaterialGroupInt = parseInt(data.results[i].MaterialGroup);
					data.results[i].CurrentStockInt = parseInt(data.results[i].CurrentStock);	
					data.results[i].StockInTransitQuantityInt = parseInt(data.results[i].StockInTransitQuantity);
					data.results[i].StockInTransitCurValInt = parseInt(data.results[i].StockInTransitCurVal);	
					// data.results[i].openlinesString = data.results[i].open_lines.toString();
					// data.results[i].sbedat = cOwner._MainController.fixDate(data.results[i].bedat);
				}
				// cOwner.getModel("JSON").setProperty("/StorageLocationArray", cOwner._MainController.removeDuplicates(aStorageLocation, 'StorageLocation'));
				// cOwner.getModel("JSON").setProperty("/ErnamArray", cOwner._MainController.removeDuplicates(aErnam, 'Ernam'));
				cOwner.getModel("JSON").setProperty("/count", data.results.length);
				// data.results.push({CurrentStockCurValSum: CurrentStockCurValSum , StockInTransitCurValSum:StockInTransitCurValSum  , type: 'SumLine'})
				cOwner.getModel("JSON").setProperty("/orders", data.results);
				cOwner.getModel("JSON").setProperty("/CurrentStockCurValSum", CurrentStockCurValSum);
				cOwner.getModel("JSON").setProperty("/StockInTransitCurValSum", StockInTransitCurValSum);
			}).catch(function (error) {
				try {
					MessageBox.error(JSON.parse(error.responseText).error.message.value);
				} catch (e) {
					MessageBox.error(JSON.stringify(error));
				}
				console.log(error);
			});
		
		},
		removeDuplicates: function(array , key){
			let lookup = new Set();
			return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
		},
		fixDate: function(oDate) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "dd.MM.yyyy"
			});
			var sNewDate = dateFormat.format(oDate);
			return sNewDate;
		},
		_filter: function() {
			var oFilter = null;
			if (this._oGlobalFilter && this._oPriceFilter) {
				oFilter = new sap.ui.model.Filter([this._oGlobalFilter, this._oPriceFilter], true);
			} else if (this._oGlobalFilter) {
				oFilter = this._oGlobalFilter;
			} else if (this._oPriceFilter) {
				oFilter = this._oPriceFilter;
			}
			
			cOwner._MainController.byId("dataTable").getBinding("rows").filter(oFilter, "Application");
			var aRows = cOwner._MainController.byId("dataTable").getBinding("rows").aIndices;
			var CurrentStockCurValSum = 0,
				StockInTransitCurValSum = 0;
			for(var i = 0; i< aRows.length; i++){
				CurrentStockCurValSum+= parseInt(cOwner.getModel("JSON").getProperty("/orders/" + aRows[i] + "/CurrentStockCurVal"));
				StockInTransitCurValSum+= parseInt(cOwner.getModel("JSON").getProperty("/orders/" + aRows[i] + "/StockInTransitCurVal"));
			}
			cOwner.getModel("JSON").setProperty("/CurrentStockCurValSum", CurrentStockCurValSum);
			cOwner.getModel("JSON").setProperty("/StockInTransitCurValSum", StockInTransitCurValSum);
			cOwner.getModel("JSON").setProperty("/count", cOwner._MainController.byId("dataTable").getBinding("rows").getLength());
		},
		onTableFilter: function(oEvent , start){
			debugger;
			cOwner.getModel("JSON").setProperty("/count", cOwner._MainController.byId("dataTable").getBinding("rows").getLength());
			if(!start){
			setTimeout(function(){ cOwner._MainController.onTableFilter("" , true) }, 1000);
			}
			if(start){
				var aRows = cOwner._MainController.byId("dataTable").getBinding("rows").aIndices;
				var CurrentStockCurValSum = 0,
				StockInTransitCurValSum = 0;
				for(var i = 0; i< aRows.length; i++){
				CurrentStockCurValSum+= parseInt(cOwner.getModel("JSON").getProperty("/orders/" + aRows[i] + "/CurrentStockCurVal"));
				StockInTransitCurValSum+= parseInt(cOwner.getModel("JSON").getProperty("/orders/" + aRows[i] + "/StockInTransitCurVal"));
				}
				cOwner.getModel("JSON").setProperty("/CurrentStockCurValSum", CurrentStockCurValSum);
				cOwner.getModel("JSON").setProperty("/StockInTransitCurValSum", StockInTransitCurValSum);
			}
			
		},
		filterGlobally: function(oEvent) {
			this._oGlobalFilter = null;
			var oFilters = [];
			var model = cOwner.getModel("JSON"),
				fData = model.getProperty("/Filters");
			if (fData.Matnr && fData.Matnr.length) {
				var aMatnr =[];
					for (const element of fData.Matnr) {
						aMatnr.push(new sap.ui.model.Filter("Material", "EQ", element.key));
					}
				oFilters.push(new Filter(aMatnr,false));
			}
			if (fData.Lgort && fData.Lgort.length) {
				var aLgort = [];
				for (const element of fData.Lgort) {
					aLgort.push(new sap.ui.model.Filter("StorageLocation", "EQ", element.key));
				}
				oFilters.push(new Filter(aLgort, false));
			}	
			if (fData.Matkl && fData.Matkl.length) {
				var aMatkl = [];
				for (const element of fData.Matkl) {
					aMatkl.push(new sap.ui.model.Filter("MaterialGroup", "EQ", element.key));
				}
				oFilters.push(new Filter(aMatkl, false));
			}
			if (oFilters.length !== 0) {
				// oFilters.push(new sap.ui.model.Filter("StockInTransitCurValSum", "Any", ""));
				// oFilters.push(new sap.ui.model.Filter("CurrentStockCurValSum", "Any", ""));
				this._oGlobalFilter = new Filter(oFilters, true);
				
			}

			this._filter();
		},
		
		clearAllFilters: function(oEvent) {
			var oTable = cOwner._MainController.byId("dataTable");
			this._oGlobalFilter = null;
			this._oPriceFilter = null;
			this._filter();
			var aColumns = oTable.getColumns();
			for (var i = 0; i < aColumns.length; i++) {
				oTable.filter(aColumns[i], null);
				oTable.sort(aColumns[i], null);
			}
			cOwner.getModel("JSON").setProperty("/Filters", []);
		},
		
		getTableFilters: function () {
			var oTable = cOwner._MainController.byId("dataTable");
			var oColumns = oTable.getColumns();
			var aFilters = [];
			for (var i = 0; i < oColumns.length; i++) {
				var sValue = oColumns[i].getFilterValue();
				if (sValue) {
					aFilters.push({
						value: sValue,
						property: oColumns[i].getFilterProperty()
					});
				}
			}
			return aFilters;
		},
		setTableFilters: function (tableFilters) {
			var oTable = cOwner._MainController.byId("dataTable");
			var oColumns = oTable.getColumns();
			for (var i = 0; i < oColumns.length; i++) {
				for (var j = 0; j < tableFilters.length; j++) {
					if (tableFilters[j].property === oColumns[i].getFilterProperty()) {
						oColumns[i].filter(tableFilters[j].value);
						continue;
					}
				}
			}
		},
		createColumnConfig: function() {
			var aCols = cOwner._MainController.byId("dataTable").getColumns();
			var aColumns = [];
			for (var i = 0; i < aCols.length; i++) {
				if (cOwner._MainController.byId("dataTable").getColumns()[i].getVisible()) {
					aColumns.push({
						label: aCols[i].getLabel().getProperty("text") ||" ",
						property: aCols[i].getFilterProperty()|| " ",
						width: '15rem'
					});
				}
			}
			return aColumns;

		},
		onExport: function() {
			var aCols, aItems, oSettings, oSheet;
			aCols = this.createColumnConfig();
			aItems = cOwner.getModel("JSON");
			var aIndices = cOwner._MainController.byId("dataTable").getBinding("rows").aIndices;
			var aSelectedModel = [];
			for (var i = 0; i<aIndices.length; i++){
				aSelectedModel.push(aItems.getProperty("/orders/" + aIndices[i] ));
			}
			// var aSelectedModel = cOwner._MainController.byId("dataTable").getBinding("rows").oList;
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedModel,
				fileName: cOwner.getModel("JSON").getProperty("/ExportName")
			};

			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function() {});

		},
		showDialog: function (sDialogName) {
			if (!this[sDialogName]) {
				this[sDialogName] = sap.ui.xmlfragment("ZMM_STOCK_VALUE.view.fragments." + sDialogName, this);
				this.getView().addDependent(this[sDialogName]);
			}
			this[sDialogName].open();
		},
		closeDialog: function (sDialogName) {
			this[sDialogName]._dialog.close();
			// this[sDialogName].destroy();
			// delete(this[sDialogName]);
		},
		onConfirm: function (oEvent , sType) {
			const aSelected = oEvent.getParameter("selectedItems");
			const model = cOwner.getModel("JSON");
			var aTokens = this.CreateToken(aSelected, sType, "SH");
			// for (var i = 0; i < aTokens.length; i++) {
			// 	var object = aSelected[i].getBindingContext("SH").getObject();
			// 	aTokens[i].StorageLocation = object.Lgort;
			// }
			model.setProperty("/Filters/" + sType, aTokens);
			this.closeDialog(oEvent);
		},
		search: function (oEvent , sType) {
			const value = oEvent.getParameter("value");
			var oFilter;
			if(sType === 'Lgort'){
				oFilter = new Filter({
				filters: [new Filter("Lgort", "Contains", value),
					new Filter("Lgobe", "Contains", value)
				],
				and: false
			});
		}
		else if(sType === 'Matkl'){
			oFilter = new Filter({
			filters: [new Filter("Matkl", "Contains", value),
				new Filter("Wgbez", "Contains", value)
			],
			and: false
		});
	}
		else if(sType === 'Matnr'){
			oFilter = new Filter({
				filters: [new Filter("Matnr", "Contains", value),
					new Filter("Maktx", "Contains", value)
				],
				and: false
			});
		}
			oEvent.getSource().getBinding("items").filter([oFilter]);
		},
		CreateToken: function (aSelected, sKey, sModel) {
			var aTokens = [];
			for (var i = 0; i < aSelected.length; i++) {
				var object = aSelected[i].getBindingContext(sModel).getObject();
			if(sKey === 'Lgort'){
				aTokens.push({
					text: object['Lgobe'],
					key: object['Lgort']});
			}
			else if(sKey === 'Matkl'){
				aTokens.push({
					text: object['Wgbez'],
					key: object['Matkl']});
			}
			else if(sKey === 'Matnr'){
				aTokens.push({
					text: object['Maktx'],
					key: object['Matnr']
				});
			}
		}
			return aTokens;
			
		},
		onDeleteToken: function (oEvent, sPath) {
			debugger;
			const sKey = oEvent.getParameter("token").getBindingContext("JSON").getObject().key;
			const model = cOwner.getModel("JSON");
			var aTokens = model.getProperty("/Filters/" + sPath);
			const index = this.findIndexToDelete(aTokens, sKey);
			aTokens.splice(index, 1);
			model.setProperty("/Filters/" + sPath, aTokens);
			// this._filter();
		},
		findIndexToDelete: function (aTokens, sKey) {
			for (var i = 0; i < aTokens.length; i++) {
				if (sKey === aTokens[i].key) {
					return i;
				}
			}
		},

		onPersoButtonPressed: function (oEvent) {
			cOwner._MainController._oTPC.openDialog();
		},

		GoToTransaction : function(sMaterial, sPlant , sStorageLocation) {
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			var semamticActionObj = "ZMM_STOCK_VALUE_MB52-display";
			var oParams = {
				IvMaterial: sMaterial,
				IvPlant: sPlant,
				IvStorageLocation: sStorageLocation
			};
			// this.setAppState(oCrossAppNavigator);
			oCrossAppNavigator.isIntentSupported([semamticActionObj])
				.done(function(aResponses) {
					if (aResponses[semamticActionObj].supported === true) {
						var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
							target: {
								semanticObject: "ZMM_STOCK_VALUE_MB52",
								action: "display"
							},
							params: oParams
						})) || "";
						oCrossAppNavigator.toExternal({
							target: {
								shellHash: hash
							}
						});
					}

				})
				.fail(function() {});
		}

	});
});